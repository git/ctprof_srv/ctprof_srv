ctprof_srv  

Version 1.1
Release Date: 9/12/2013 

Description
===========

Ctprof provides system level bus profiling use cases for any slave bus in
the device that has a CP Tracer. Ctprof is made up of two components; ctprof
which is the host client application, and ctprof_srv which is the target
server application. The client is delivered with TI's CCS emupacks (see
System Requirements for minimum emupack revision for this release) and can be
installed independent of CCS. The server is delivered as source with an example
application which also contains a small "C" file that can be integrated with
your application for coordinating ctprof_srv's trace collection activity.

Organization
============

The ctools_srv project is delivered as source in three directories:

    server
    common
    example_app

Both the server and example_app have makefiles. 

Make & Installation
===================

If using SC-MCSDK 02.02.00 then ctprof_srv, ctprof_ex, and ctprof_sync.sh are
pre-installed in the /usr/bin directory. Otherwise, the server and example_app
directories contain makefiles that utilizes the CROSS_COMPILE environment
variable. Both release and debug versions can be made. The executable is
saved in the release or debug directories. 

Examples:

make with CROSS_COMPILE:
make clean debug install arm DESTDIR=/your/favorite/utility/dir
make clean release install arm DESTDIR=/your/favorite/utility/dir

Make with your favorite compiler:
make CC=target-gcc clean debug install DESTDIR=/your/favorite/utility/dir
make CC=target-gcc clean release install DESTDIR=/your/favorite/utility/dir

make all with CROSS_COMPILE:
make all arm DESTDIR=/your/favorite/utility/dir (same as: make clean release install
 arm DESTDIR=/your/favorite/utility/dir)

System Requirements
===================

- A Host Linux machine (Ubuntu 10.4 or higher) with emupack M8 installed.
- A TCI6414 EVM or equivalent target system with a working network connection
  (TCP is used).
- Requires the SC-MCSDK 02.02.00 Linux image.

Documentation
=============

The server should simply be run in the background from the console:

$ctprof_srv &

Use -h to see options.

The ctprof utility is then used from a host to connect to the server and issue
use case commands (like measure utilized bandwidth or latency of a specific
slave) that are serviced by ctprof_srv through a socket interface. 

See http://processors.wiki.ti.com/index.php/Ctprof for documentation and
examples.

History
=======

8/21/2013 - First release
9/12/2013 - Version 1.1

Known Deficiencies
==================

Enhancements & New Features
===========================

Version 1.1 update                                
- Made minor ctprof_srv modification to eliminate pipe warning                 
  when terminating (when using -t with ctprof_sync.sh script).
- Improved ctprof_ex to make it usable for all examples in wiki              
  documentation.

Bug Fixes
=========

End of Document



