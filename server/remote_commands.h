/*
 * remote_commands.h
 *
 * Ctools Profiler Server Implementation
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef REMOTE_COMMANDS_H
#define REMOET_COMMANDS_H

typedef enum {
    OP_MODE, 
    DURATION,
    START_DELAY,
    ETB_ENABLE,
    ETB_MODE,
    LAST_SET_ELEMENT
} set_element_t;

/* Note: all *_command_handler functions are meant to be called only by the command_handler. */
/* All other functions may be used anywhere needed */
void set_command_handler(int argc, char *argv[], char * rbuf, size_t rsp_size);
void mmap_command_handler(int argc, char *argv[], char * rbuf, size_t rsp_size);
void ummap_command_handler(int argc, char *argv[], char * rbuf, size_t rsp_size);
void version_command_handler(int argc, char *argv[], char * rbuf, size_t rsp_size);
void modcntl_add_command_handler(int argc, char *argv[], char * rbuf, size_t rsp_size);
void modcntl_remove_command_handler(int argc, char *argv[], char * rbuf, size_t rsp_size);
void start_command_handler(int argc, char *argv[], char * rbuf, size_t rsp_size);
void end_command_handler(int argc, char *argv[], char * rbuf, size_t rsp_size);
void status_command_handler(int argc, char *argv[], char * rbuf, size_t rsp_size);
void debug_command_handler(int argc, char *argv[], char * rbuf, size_t rsp_size);


void get_set_value(set_element_t element, unsigned int *value, bool *is_valid);
int remote_memory_write(uint32_t addr, size_t byte_cnt, uint32_t * pbuf);
int remote_memory_read(uint32_t addr, size_t byte_cnt, uint32_t * pbuf);
void remote_pipe_write(const char * wr_buf_p, size_t wr_bytecnt, char * broken_pipe_msg);

void start_recording();
void stop_recording();
#endif
