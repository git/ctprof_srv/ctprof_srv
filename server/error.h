/*
 * error.h
 *
 * Ctools Profiler Server Implementation
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#ifndef ERROR_H
#define ERROR_H

#include <stdbool.h>

struct error_info_t {
    char *msg;
    bool fatal;
};

typedef enum  { ERR_NONE,
                ERR_MEM_ALLOC,
                ERR_PARSER, 
                ERR_OPT_ARG,
                ERR_LOG_FILE,
                ERR_SOCKET_CREATE,
                ERR_SOCKET_BIND,
                ERR_SOCKET_LISTEN,
                ERR_SOCKET_ACCEPT,
                ERR_SOCKET_WRITE,
                ERR_SOCKET_READ,
                ERR_SOCKET_CONNECT,
                ERR_SOCKET_DISCONNECT,
                ERR_SOCKET_SEND,
                ERR_SOCKET_RECV,
                ERR_SOCKET_READY,
                ERR_SOCKET_OPTION,
                ERR_SOCKET_FLAGS,
                ERR_SOCKET_MAX,
                ERR_RSP_INVALID,
                ERR_MMAP_OPEN,
                ERR_MMAP_FAIL,
                ERR_MUNMAP_FAIL,
                ERR_INVALID_SIGNAL_CAUGHT,
                ERR_SEM_OPEN,
                ERR_ETB_ISSUE,
                ERR_ETB_OVERFLOW,
                ERR_FIFO_MAKE,
                ERR_FIFO_OPEN,
                ERR_DEBUG
}err_id_t;

extern struct error_info_t error_info[];

#endif
