/*
 * error.c
 *
 * Ctools Profiler Server Implementation
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "error.h"

struct error_info_t error_info[] = {
    [ERR_NONE] = {"No error", false},
    [ERR_MEM_ALLOC] = {"Memory Allocation Error", true},
    [ERR_PARSER] =    {"Invalid option - try --help", true},
    [ERR_OPT_ARG] = {"Invalid option argument - try --help", false},
    [ERR_LOG_FILE] = {"Can not open ctprof_srv_log.txt", false},
    [ERR_SOCKET_CREATE] = {"Error opening socket - try a different port", true},
    [ERR_SOCKET_BIND] = {"Error binding socket - try a different port", true},
    [ERR_SOCKET_LISTEN] = {"Error listening socket - try a different port", true},
    [ERR_SOCKET_ACCEPT] = {"Error accepting client - try a different port", true},
    [ERR_SOCKET_WRITE] = {"Can not write to client", true},
    [ERR_SOCKET_READ] = {"Can not read from client", true},
    [ERR_SOCKET_CONNECT] = {"Socket connection failure", true},
    [ERR_SOCKET_DISCONNECT] = {"Socket disconnected", true},
    [ERR_SOCKET_SEND] = {"Socket error on send", true},
    [ERR_SOCKET_RECV] = {"Socket error on receive", true},
    [ERR_SOCKET_READY] = {"Error while waiting for socket to become ready", true},
    [ERR_SOCKET_OPTION] = {"Error while trying to modify socket options - try a different port", true},
    [ERR_SOCKET_FLAGS] = {"Error modifying socket file descriptor flags", true},
    [ERR_SOCKET_MAX] = {"Maximum number of open sockets exceeded", true},
    [ERR_RSP_INVALID] = {"Invalid command received", true},
    [ERR_MMAP_OPEN] = {"Can not open '/dev/mem' with O_RDWR | O_SYNC | O_RSYNC attributes", true},
    [ERR_MMAP_FAIL] = {"Can not map region", true},
    [ERR_MUNMAP_FAIL] = {"Can not unmap region", true},
    [ERR_INVALID_SIGNAL_CAUGHT] = {"Invalid signal caught", true},
    [ERR_SEM_OPEN] = {"Semaphore open error", true},
    [ERR_ETB_ISSUE] = {"ETB Library error - see log file for details", true},
    [ERR_ETB_OVERFLOW] = {"ETB overflow error", false},
    [ERR_FIFO_MAKE] = {"FIFO file make error", true},
    [ERR_FIFO_OPEN] = {"FIFO file open error", true},   
    [ERR_DEBUG] = {"Software error", true}

};
