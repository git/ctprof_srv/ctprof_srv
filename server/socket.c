/*
 * socket.c
 *
 * Ctools Profiler Server Implementation
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/time.h>
#include "error_handler.h"
#include "logging.h"
#include "socket.h"
#include "ctoolsprof.h"

/* Define empty set of socket file descriptors for select */
static fd_set read_fds;
static fd_set write_fds;
static int socket_cnt = 0;

/* Define socket registration table */
struct socket_table_t {
    int fd;
    bool is_read;         /* True for read fd, false for write fd */
    bool is_valid;
};

/* Command, logging and 9 ETBs */
#define MAX_SOCKETS 11

static struct socket_table_t socket_table[MAX_SOCKETS];

/***************************************************************************** 
 *  Public functions
 *****************************************************************************/

/***************************************************************************** 
 *  socket_open()
 *
 *  This function needs to be called once to initialize the socket interface.
 *
 *  - Initialize the read and write file descriptor sets
 *  - Initialize the socket table.
 *  
 *****************************************************************************/
void socket_open()
{
    LOGFUNC();
#if DEBUG
    if (MAX_SOCKETS >= FD_SETSIZE) {
        err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
    }
#endif

    FD_ZERO(&read_fds);
    FD_ZERO(&write_fds);

    for (int i = 0; i < MAX_SOCKETS; i++) {
        socket_table[i].is_valid = false;
    }

}

/***************************************************************************** 
 *  socket_server_create()
 *
 *  Create a socket between this server and a client
 *
 *  - Create and initialize the socket. 
 *  - Bind the socket to the port.
 *  - Listen for a connection from the client to the socket.
 *  - Accept the connection.
 *  - Close the listening socket.
 *  - Return the client socket file descriptor.
 *
 *  - If an error on the socket operation is detected the error handler is called. 
 *    Note: All socket errors are fatal, which causes the server to exit.
 *  
 *****************************************************************************/
int socket_server_create(int port)
{
    LOGFUNC();
    LOGMSG1("%s:create socket for port %d", __func__, port);

    int socket_fd, client_socket_fd;
    struct sockaddr_in server, client;
    socklen_t client_size = sizeof(client);
    int reuse_addr = 1;

    if (socket_cnt == MAX_SOCKETS) {
        err_handler(ERR_TYPE_SYSTEM, ERR_SOCKET_MAX);
    }

    /* Create and initialize the socket. */
    socket_fd = socket(PF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0) {
        err_handler(ERR_TYPE_SYSTEM, ERR_SOCKET_CREATE);
    }

    if(setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &reuse_addr, sizeof reuse_addr) == -1) {
        err_handler(ERR_TYPE_SYSTEM, ERR_SOCKET_OPTION);
    }

    LOGMSG1("%s:socket file descriptor opened for AF_INET & SOCK_STERAM", __func__);

    memset(&server,0,sizeof(struct sockaddr_in));
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = htons((uint16_t)port);

    /* Bind the socket to the port. */
	if(bind(socket_fd, (struct sockaddr *)&server, sizeof(server)) < 0) {
        err_handler(ERR_TYPE_SYSTEM, ERR_SOCKET_BIND);
	}

    /* Listen for a connection from the client to the socket. */
	if (listen(socket_fd, 1) < 0) {
        err_handler(ERR_TYPE_SYSTEM, ERR_SOCKET_LISTEN);
	
	}   

    /* Accept the connection. */
    memset(&client,0,sizeof(struct sockaddr_in));
	client_socket_fd = accept(socket_fd, (struct sockaddr *)&client, 
                              &client_size);
	if(-1 == client_socket_fd) {
        LOGMSG1("%s:client accept error is %d", __func__, client_socket_fd);
		err_handler(ERR_TYPE_SYSTEM, ERR_SOCKET_ACCEPT);
	} 

    LOGMSG2("%s:client connect accepted client_socket_fd is %d", __func__, client_socket_fd);

    /* Close the listening socket. */
    close(socket_fd);

    /* Increment the socket count */
    socket_cnt++;

    /* Return the client socket file descriptor. */
    return client_socket_fd;
}

/***************************************************************************** 
 *  socket_command_optimize()
 *
 *  Optimize the command socket.
 *****************************************************************************/
void socket_command_optimize(int socket_fd)
{
    int on = 1;

    if(setsockopt(socket_fd, SOL_TCP, TCP_NODELAY, &on, sizeof(on)) == -1) {
        err_handler(ERR_TYPE_SYSTEM, ERR_SOCKET_OPTION);
    }   
    
}


/***************************************************************************** 
 *  socket_recv_data()
 *
 *  Get a buffer of data from a socket file descriptor.
 *
 *  Note: This function is non-blocking safe. 
 * 
 *  - If the call to recv() executes successfully buf_retlen will be non-zero
 *    and the function returns 0.
 *  - If socket_fd has been set to non-blocking and no data is available
 *    EAGAIN is returned.
 *  - If an error on the socket is detected the error handler is called. 
 *    Note: All socket errors are fatal, which causes the server to exit.
 *  - If a client disconnect is detected, the global disconnect state is set,
 *    buf_retlen is set to 0, and the function returns 0.
 * 
 *****************************************************************************/
int socket_recv_data(int socket_fd, uint8_t* buf, size_t buf_len, 
                    size_t buf_offset, size_t* buf_retlen)
{
    char* pbuf = (char*)buf + buf_offset;
    int length = buf_len - buf_offset;

#if DEBUG
	if ((buf == NULL) || (buf_retlen == NULL)) {
        err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
	}
#endif

	*buf_retlen = recv(socket_fd, pbuf, (int)length, 0);
    
    if ((-1 == (int) *buf_retlen )
        && ((errno == EAGAIN) || (errno == EWOULDBLOCK) || (errno == EINTR))) {
        return EAGAIN;
    }

    if ((-1 == (int) *buf_retlen ) 
        && ((errno == ECONNRESET) || (errno == ECONNABORTED) || errno == EPIPE)) {
        err_handler(ERR_TYPE_SYSTEM, ERR_SOCKET_CONNECT);
    }

    if (-1 == (int) *buf_retlen ) {
        err_handler(ERR_TYPE_SYSTEM, ERR_SOCKET_RECV);
    }

	if(0 == *buf_retlen) {
        LOGMSG2("%s:disconnect detected - setting global_state.socket_disconnect true", __func__);
        global_state.socket_disconnect = true; 
	}

	return 0;

}

/***************************************************************************** 
 *  socket_send_data()
 *
 *  Send a buffer of data to a socket file descriptor.
 *
 *  Note: This function is non-blocking safe. 
 * 
 *  - If the call to send() executes successfully buf_retlen will indicate the
 *    number of bytes sent from buf and the function returns 0.
 *  - If socket_fd has been set to non-blocking and data can not be sent
 *    EAGAIN is returned.
 *  - If an error on the socket is detected the error handler is called. 
 *    Note: All socket errors are fatal, which causes the server to exit.
 * 
 *****************************************************************************/
int socket_send_data(int socket_fd, uint8_t* buf, size_t buf_len, 
                    size_t buf_offset, size_t* buf_retlen)
{
	const char* pbuf = (const char*)buf + buf_offset;
	*buf_retlen =0;

#if DEBUG
	if ((buf == NULL) || (buf_retlen == NULL)) {
        err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
	}
#endif

	*buf_retlen = send(socket_fd, pbuf, (int)buf_len, 0);

    if ((-1 == (int) *buf_retlen ) 
        && ((errno == EAGAIN) || (errno == EWOULDBLOCK) || (errno == EINTR))) {
        return EAGAIN;
    }

    if ((-1 == (int) *buf_retlen ) 
        && ((errno == ECONNRESET) || (errno == ECONNABORTED) || errno == EPIPE)) {
        err_handler(ERR_TYPE_SYSTEM, ERR_SOCKET_CONNECT);
    }

    if (-1 == (int) *buf_retlen ) {
        err_handler(ERR_TYPE_SYSTEM, ERR_SOCKET_SEND);
    }

	return 0;

}

/***************************************************************************** 
 *  socket_add_list()
 *
 *  - Add a socket file descriptor to the socket table. 
 *
 *****************************************************************************/
void socket_add_list(int socket_fd, bool is_read)
{
    LOGFUNC();

    int i, first_invalid;
    bool first_invalid_found = false;

#if DEBUG
    if (socket_fd >= FD_SETSIZE) {
        err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
    }
#endif

    for (i = 0; i < MAX_SOCKETS; i++) {
       
        if ((socket_table[i].is_valid) && (socket_table[i].fd == socket_fd)){
            return;
        }
        if ((!socket_table[i].is_valid) && (!first_invalid_found)) {
            first_invalid = i;
            first_invalid_found = true;
        }                  
    }

#if DEBUG
    if (!first_invalid_found) {
        err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
    }
#endif

    socket_table[first_invalid].is_valid = true;
    socket_table[first_invalid].is_read = is_read;
    socket_table[first_invalid].fd = socket_fd;

    LOGMSG1("%s:add socket %d to socket_table position %d", __func__, socket_fd, first_invalid);

}

/***************************************************************************** 
 *  socket_remove_list()
 *
 *  - Remove a socket file descriptor from the socket table. 
 *
 *****************************************************************************/
void socket_remove_list(int socket_fd)
{
    LOGFUNC();
    int i;

    for (i = 0; i < MAX_SOCKETS; i++) {
        if ((socket_table[i].is_valid) && (socket_table[i].fd == socket_fd)){
            socket_table[i].is_valid = false;
            LOGMSG1("%s:remove socket %d from socket_table position %d", __func__, socket_fd, i);
            break;
        }                  
    }

    if (i == MAX_SOCKETS) {
        LOGMSG1("%s:Did not find socket in list - this may not be a problem", __func__);
    }
    
}

/***************************************************************************** 
 *  socket_check_read_list()
 *
 *  - Return true if this socket is in the set of read sockets that have
 *    data available. 
 *
 *****************************************************************************/
int socket_check_read_list(int socket_fd)
{
    return FD_ISSET (socket_fd, &read_fds);

}

/***************************************************************************** 
 *  socket_check_write_list()
 *
 *  - Return true if this socket is in the set of write sockets that are ready
 *    to send more data. 
 *
 *****************************************************************************/
int socket_check_write_list(int socket_fd)
{
    return FD_ISSET (socket_fd, &write_fds);

}

/***************************************************************************** 
 *  socket_set_nonblocking()
 *
 *  - Set the socket file descriptor non-blocking. 
 *
 *****************************************************************************/
void socket_set_nonblocking(int socket_fd)
{
    LOGFUNC();   
    int flags = fcntl(socket_fd, F_GETFL, 0);
    if (flags < 0) {
        err_handler(ERR_TYPE_SYSTEM, ERR_SOCKET_FLAGS);
    }

    flags = fcntl(socket_fd, F_SETFL, flags | O_NONBLOCK);
    if (flags < 0) {
        err_handler(ERR_TYPE_SYSTEM, ERR_SOCKET_FLAGS);
    }
}

/***************************************************************************** 
 *  socket_wait()
 *
 *  Wait for either a 1 msec timeout or for a read socket with data available 
 *  or a write socket that is ready for data.
 *
 *  - Set the timeout struct.
 *  - Set the read and write file descriptor sets.
 *  - Wait for a file descriptor event or timeout. 
 *  - Returns 0 for timeout, or the number of file descriptors with events pending
 *    (number of read_fds with data ready to read and write_fds that are ready for
 *     more data).
 *
 *****************************************************************************/
int socket_wait()
{
    int activity, high_socket = 0;

    /* Set the timeout struct */
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 1000; 

    FD_ZERO(&read_fds);
    FD_ZERO(&write_fds);

    /* Set the read and write file descriptor sets. */
    for (int i = 0; i < MAX_SOCKETS; i++) {
        if ((socket_table[i].is_valid) && (socket_table[i].is_read)) {
            FD_SET(socket_table[i].fd, &read_fds);
            if (socket_table[i].fd > high_socket) {
                high_socket = socket_table[i].fd;
            }
        }
        if ((socket_table[i].is_valid) && (!socket_table[i].is_read)) {
            FD_SET(socket_table[i].fd, &write_fds);
            if (socket_table[i].fd > high_socket) {
                high_socket = socket_table[i].fd;
            }
        }
    }

    /* Wait for a file descriptor event or timeout. */
    do {
        activity = select(high_socket+1, &read_fds, &write_fds, NULL, &timeout);
    } while( activity == -1 && errno == EINTR);

    if (activity < 0) {
        err_handler(ERR_TYPE_SYSTEM, ERR_SOCKET_READY);
    }

    /* if activity 0 timeout, else activity must be => 1 - number of read_fds and write_fds
       ready to read */
    return activity;
}   

/***************************************************************************** 
 *  socket_close()
 *
 *  - All done so close the socket. 
 *
 *****************************************************************************/
void socket_close(int fd)
{
    LOGFUNC();
    /* Decrement the socket count */
    socket_cnt--;
    close(fd);
}

