/*
 * signal_handler.c
 *
 * Ctools Profiler Server Implementation
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <signal.h>
#include <unistd.h>
#include "error_handler.h"
#include "logging.h"
#include "ctoolsprof.h"
#include "signal_handler.h"

static recording_op start_recording;
static recording_op stop_recording;

struct sigaction action;

/***************************************************************************** 
 *  Private Functions
 *****************************************************************************/
/***************************************************************************** 
 *  signal_handler()
 *
 *  This is a common signal handler for all signals
 *
 *  - If the signal is not expected then generate a fatal error and exit.
 *  - Handle the signal.
 *    - If SIGALRM and delay_active true, then start recording.
 *    - If SIGALRM and duration_active true, then stop recording.
 *    - If SIGUSR1 and signal_active true, then start recording.
 *    - If SIGUSR2 and signal_active true, then stop recording.
 *    - if SIGINT and recording then stop recording.
 *  
 *****************************************************************************/
void signal_handler(int sig) {

    LOGFUNC();

    LOGMSG1("%s:Caught signal %d", __func__, sig); 

    /* If the signal is not expected then generate a fatal error and exit. */
    if ((global_state.delay_active == false)
        && (global_state.recording == STATE_UNINITIALIZED)) {
        
        LOGMSG1("%s:caught signal %d while in uninitialized state", __func__,
                signal);
        err_handler(ERR_TYPE_LOCAL, ERR_INVALID_SIGNAL_CAUGHT);

    }

    /* Handle the signal */
    switch (sig) {
    case SIGALRM:
        if (global_state.delay_active == true) {
            LOGMSG1("%s:caught SIGALRM - calling start_recording()", __func__);
            global_state.delay_active = false;
            global_state.duration_active = true;
            sigaction(SIGALRM, &action, NULL);
            start_recording();
        } else if (global_state.duration_active == true) {
            LOGMSG1("%s:caught SIGALRM - calling end_recording()", __func__);
            global_state.duration_active = false;
            sigaction(SIGALRM, &action, NULL);
            stop_recording();
        }
        break;
    case SIGUSR1:
        if ((global_state.recording != STATE_RECORDING) 
            && (global_state.signal_active == true)) {     
            LOGMSG1("%s:caught SIGUSR1 - calling start_recording()", __func__);
            sigaction(SIGUSR1, &action, NULL);
            start_recording();        
        }
        break;
    case SIGUSR2:
        if ((global_state.recording == STATE_RECORDING) 
            && (global_state.signal_active == true)) {     
            LOGMSG1("%s:caught SIGUSR2 - calling end_recording()", __func__);
            sigaction(SIGUSR2, &action, NULL);
            stop_recording();
            global_state.signal_active = false;
        }
        break;
    case SIGINT:
        if (global_state.recording == STATE_RECORDING) {     
            LOGMSG1("%s:caught SIGINT - calling end_recording()", __func__);
            sigaction(SIGINT, &action, NULL);
            stop_recording();           
        }
        break;
    case SIGPIPE:
        /* Client has broken the pipe - so simply stop using it */
        LOGMSG1("%s:caught SIGPIP - close the pipe", __func__);
        g_fifo_enabled = false;
        if (g_fifo_fd != -1) {
            close(g_fifo_fd);
            g_fifo_fd = -1;
        }
        break;
    default:
        LOGMSG1("%s:caught invalid signal %d", __func__, signal);
        err_handler(ERR_TYPE_LOCAL, ERR_INVALID_SIGNAL_CAUGHT);
    
    } /* End of switch */
}

/***************************************************************************** 
 *  Public Functions
 *****************************************************************************/
/***************************************************************************** 
 *  signal_handler_init()
 *
 *  Initialize the signal handling
 *
 *  Note: This function takes the start_recording and stop_recording functions
 *  as inputs eliminating the need to include remote_commands.h. 
 *  
 *****************************************************************************/
void signal_handler_init(recording_op start_op, recording_op stop_op)
{
    LOGFUNC();

    sigset_t block_mask;
        
    start_recording = start_op;
    stop_recording = stop_op;

    memset(&action, 0, sizeof(struct sigaction));

    /* Setup signals to be masked while servicing a signal*/
    sigemptyset(&block_mask);
    sigaddset(&block_mask, SIGALRM);
    sigaddset(&block_mask, SIGUSR1);
    sigaddset(&block_mask, SIGUSR2);
    sigaddset(&block_mask, SIGINT);

    action.sa_handler = signal_handler;
    action.sa_mask = block_mask;
    
    /*Register signal handlers */
    sigaction(SIGALRM, &action, NULL);
    sigaction(SIGUSR1, &action, NULL);
    sigaction(SIGUSR2, &action, NULL);
    sigaction(SIGINT, &action, NULL);
    sigaction(SIGPIPE, &action, NULL);

}

/***************************************************************************** 
 *  signal_handler_block()
 *
 *  Sets signals to block.
 *
 *  Note: This function saves the original signal blocking mask in orig_mask.
 *  
 *****************************************************************************/
static sigset_t orig_mask;
static bool signals_blocked = false;

void signal_handler_block()
{
    if (signals_blocked == true) {
        return;
    }

    sigset_t block_mask;

    sigemptyset(&block_mask);
    sigaddset(&block_mask, SIGALRM);
    sigaddset(&block_mask, SIGUSR1);
    sigaddset(&block_mask, SIGUSR2);
    sigaddset(&block_mask, SIGINT);

	if (sigprocmask(SIG_BLOCK, &block_mask, &orig_mask) < 0) {
        err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
	}

    signals_blocked = true;

}
/***************************************************************************** 
 *  signal_handler_unblock()
 *
 *  Restores original signal state prior to blocking.
 *
 *  Note: Do not call this function without calling signal_handler_block() first.
 *  
 *****************************************************************************/
void signal_handler_unblock()
{
    if (signals_blocked == false) {
        return;
    }

	if (sigprocmask(SIG_SETMASK, &orig_mask, NULL) < 0) {
        err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
	}

    signals_blocked = false;
    
}

