/*
 * etb_handler.c
 *
 * Ctools Profiler Server Implementation
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include "ctoolsprof.h"
#include "error_handler.h"
#include "logging.h"
#include "etb_handler.h"
#include "ETBInterface.h"
#include "socket.h"
#include <arpa/inet.h>

/***************************************************************************** 
 *  Note - The etb_handler is an abstraction layer between ctprof and ETBLib.
 *  
 *****************************************************************************/


/***************************************************************************** 
 *  Definitions, Constants and Statics
 *  
 *****************************************************************************/
static ETBHandle* etb_handle = NULL;

struct etb_queue_t {
    uint32_t * buf_start;       /* Start of the buffer - constant for life of queue element. Used to free the buffer. */
    uint32_t * buf_ptr;         /* Initialized with start of buffer, but is used to schedule partially sent buffers.  */
    int byte_cnt;
    struct etb_queue_t * next;   
};  

static struct etb_queue_t * etb_queue_head = NULL;
static struct etb_queue_t * etb_queue_tail = NULL;
static int etb_queue_cnt = 0;

static uint32_t etb_size; /* ETB size in uint32_t elements*/
static int etb_socket;    /* Socket ETB data is sent to host with */

static const int max_queue_depth = 1024;

#ifdef TEST_MODE
#include "remote_commands.h"
static FILE * test_fp;
static long test_fp_recpos = 0;
static long test_fp_maxpos = 0;
static long test_fp_dtat2send = 0;
static const int etb_word_size = sizeof(uint32_t);
#endif

static bool etb_is_enabled = false;
static bool etb_disabled_wrapped = false;
static int etb_words_queued = 0;

/***************************************************************************** 
 *  Internal Function Prototypes 
 *  - See Private Function section below for implementations )
 *****************************************************************************/
static void etb_process_data(int etb_word_cnt);
static int etb_data_available(bool * wrapped);

/***************************************************************************** 
 *  Public Functions
 *  
 *****************************************************************************/
/***************************************************************************** 
 *  etb_init()
 *
 *  Initialize the etb handler with a socket. The etb_socket is used to send
 *  queued ETB Trace data back to the client.
 *  
 *****************************************************************************/
void etb_init(int socket)
{
    LOGFUNC();
    etb_socket = socket;
}

/***************************************************************************** 
 *  etb_config()
 *
 *  Config ETBLib for the ETB type and operating mode.
 *
 *  - Determine the etb mode and core id.
 *  - If TEST_MODE not defined open the ETBLib.
 *  - If TEST_MODE set open the trace data bin file, figure out the number of
 *    bytes in the file and then setup a random number generator to size the
 *    amount of ETB data that is available as the ETB is recording. 
 *  
 *****************************************************************************/
void etb_config(etb_bufmode_t mode, etb_core_id_t core_id)
{   
    eETB_Error  etb_ret;
    eETB_Mode etb_mode;
    uint8_t etb_core_id;

    LOGFUNC();

    switch (mode) {
    case ETB_CIRCULAR:
        etb_mode = eETB_TI_Mode;
        break;
    case ETB_FIXED:
        if (core_id == SYS_TIETB) {
            etb_mode = eETB_TI_Mode_AND_Stop_Buffer;
            break;
        }
       
        LOGMSG1("%s:ETB only supports circular mode", __func__, etb_ret);
        err_handler(ERR_TYPE_LOCAL, ERR_ETB_ISSUE);    
    }

    switch (core_id) {
    case SYS_TIETB:
        etb_core_id = SYS_ETB;
        break;
    case ARM_CSETB:
        etb_core_id = SYS_ETB;
        break;
    }

    /* Initialize ETB
     *  Note - ETB_open will call cTools_memMap to map the ETB address 
     *  space to the ctoolsprof virtual space.
     */
    {
#ifndef TEST_MODE
        LOGMSG1("%s:Calling ETB_open, mode is %d, core id is %d", __func__,
                etb_mode, etb_core_id); 
        ETB_errorCallback etb_err_callback = NULL;   
        etb_ret = ETB_open(etb_err_callback, etb_mode, etb_core_id,
                           &etb_handle, &etb_size);
#else
        test_fp = fopen("cpt_etbdata.bin", "r");
        if (test_fp != NULL) {
            /* Get the position of the last byte in the file */
            fseek(test_fp, 0, SEEK_END);
            test_fp_maxpos = ftell(test_fp);
            fseek(test_fp, 0, SEEK_SET);

            srand(1);
            LOGMSG1("%s:Test file size is %d, random size max is %d", __func__,
                    test_fp_maxpos, RAND_MAX);
            etb_ret = eETB_Success;
        } else {
            etb_ret = eETB_Error_Bad_Param;  
        }
#endif

        if (etb_ret != eETB_Success) {
            LOGMSG1("%s:ETB_open error %d", __func__, etb_ret);
            err_handler(ERR_TYPE_LOCAL, ERR_ETB_ISSUE);
        } else {
            LOGMSG1("%s:ETB_open successful, size of etb is %d", __func__, etb_size);
        }
    }
}

/***************************************************************************** 
 *  etb_term()
 *
 *  Close ETBLib.
 * 
 *****************************************************************************/
void etb_term()
{
    eETB_Error  etb_ret;

    LOGFUNC();   

#ifdef TEST_MODE
    return;
#endif
    
    etb_ret  = ETB_close(etb_handle);
    if (etb_ret != eETB_Success) {

        LOGMSG1("%s:ETB_close error %d", __func__, etb_ret);
        err_handler(ERR_TYPE_LOCAL, ERR_ETB_ISSUE);
    }

}


/***************************************************************************** 
 *  etb_status()
 *
 *  - Returns the number of words available, if the ETB is enabled, 
 *    and if it has wrapped.
 *  - If etb mode is ONESHOT and recording return number of bytes available, if
 *    not recording then return the number of words queued to transfer.
 *  - If etb mode is DRAIN then always return the number of words currently 
 *    queued.
 * 
 *****************************************************************************/
int etb_status(bool * is_enabled, bool * is_wrapped)
{

    int available_etb_words = 0;

#if DEBUG
    if (is_wrapped == NULL) {
        err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
    }
#endif

    if (is_enabled != NULL) {
        *is_enabled = etb_is_enabled;
    }

    if (global_state.recording == STATE_UNINITIALIZED) {
        return 0;
    }

    switch (global_state.etb_mode) {
    case ETB_MODE_ONESHOT_FIXED:
    case ETB_MODE_ONESHOT_CIRCULAR:
    {

        if (global_state.recording == STATE_RECORDING) {
            available_etb_words = etb_data_available(is_wrapped);
        } else {
            /* Must be STATE_STOPPED */
            etb_data_available(is_wrapped);
            available_etb_words = etb_words_queued;
            etb_words_queued = 0;
        }
        break;
    }
    case ETB_MODE_DRAIN:
    {
        available_etb_words = etb_words_queued;
        etb_words_queued = 0;
        break;
    }
    }/* End of switch */

    return available_etb_words;
}

/***************************************************************************** 
 *  etb_enable()
 *
 *  - Simply enable the ETB to collect data.
 * 
 *****************************************************************************/
void etb_enable()
{
    eETB_Error  etb_ret;

    LOGFUNC();

#ifndef TEST_MODE
    etb_ret = ETB_enable(etb_handle, 0);
    if(etb_ret != eETB_Success) {

        LOGMSG1("%s:ETB_enable error %d", __func__, etb_ret);
        err_handler(ERR_TYPE_LOCAL, ERR_ETB_ISSUE);
    }
#endif

    etb_is_enabled = true;
    etb_disabled_wrapped = false;
}

/***************************************************************************** 
 *  etb_disable()
 *
 *  - Simply disable the ETB.
 * 
 *****************************************************************************/
void etb_disable()
{
    eETB_Error  etb_ret;

    LOGFUNC();

#ifndef TEST_MODE
    etb_ret = ETB_disable(etb_handle);
    if(etb_ret != eETB_Success) {

        LOGMSG1("%s:ETB_disable error %d", __func__, etb_ret);
        err_handler(ERR_TYPE_LOCAL, ERR_ETB_ISSUE);
    }

    /* Need to cache the wrapped status before any ETB_reads. */
    ETBStatus etb_status;
    etb_ret = ETB_status(etb_handle, &etb_status);
    if (etb_ret != eETB_Success) {

        LOGMSG1("%s:ETB_status error %d", __func__, etb_ret);
        err_handler(ERR_TYPE_LOCAL, ERR_ETB_ISSUE);
    }
    etb_disabled_wrapped = (etb_status.isWrapped) ? true : false;

#endif
    etb_is_enabled = false;
}

/***************************************************************************** 
 *  etb_add_queue()
 *
 *  This function queues available ETB data (all the heavy lifting is done in 
 *  etb_process_data()).
 * 
 *  - Check that the queue depth has not been exceeded - if so something is really 
 *    wrong. Should only ever be a problem for drain mode.
 *  - If limit is ETB_QUEUE_ALL then all ETB data is sent (). If limit is set to
 *    ETB_QUEUE_LIMIT then data is only sent if the amount of data available has
 *    reached 1/4 of the ETB (which for SYS_ETB is 8K).
 *****************************************************************************/
void etb_add_queue(etb_queue_limit_t limit)
{
    LOGFUNC();
 
    bool is_wrapped;
    int etb_word_cnt = etb_data_available(&is_wrapped);

    if (etb_queue_cnt == max_queue_depth) {
        LOGMSG1("%s:Max queue depth reached - this most likely indicates the "
                "trace data capture rate is to high. Try decreasing the sample "
                "rate", __func__);
        err_handler(ERR_TYPE_LOCAL, ERR_ETB_ISSUE);
    }

    /* As we read the data with etb_process_data the etb rd pointer
     * will advance toward the etb write pointer so the next time
     * we check the number of words available should be much less
     */

    if (   ((limit == ETB_QUEUE_LIMIT) && (etb_word_cnt > etb_size/4))
        || ((limit == ETB_QUEUE_ALL) && (etb_word_cnt > 0))) {

        LOGMSG1("%s:process %d ETB words", __func__, etb_word_cnt);
        etb_process_data(etb_word_cnt);
        return;
    }
   
    LOGMSG1("%s:No ETB data to process", __func__);
    
}

/***************************************************************************** 
 *  etb_send_from_queue()
 *
 *  This function takes etb data from the queue and sends it out the etb_socket.
 * 
 *  - Get the etb buffer from the head of the queue.
 *  - Send the data to the etb_socket. Note that socket_send_data() may not send
 *    all the requested data on the first try. If socket_send_data() returns non-zero
 *    the a retry is required, so the buffer pointer and bytes to send of the queue
 *    element are simply updated. 
 *  - Update the queue head.
 *****************************************************************************/
void etb_send_from_queue()
{
    LOGFUNC(); 

    struct etb_queue_t * etb_queue_element;
    size_t bytes_sent;
    int retry;

    uint8_t * send_buf;
    size_t bytes_to_send;

    /* Get the etb buffer from the head of the queue. */
    if (etb_queue_head != NULL) {
        etb_queue_element = etb_queue_head;       
    } else {
#if DEBUG
        err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
#else
        return;
#endif
    }

    etb_queue_cnt--;

    send_buf = (uint8_t *)etb_queue_element->buf_ptr;
    bytes_to_send = etb_queue_element->byte_cnt;

    /* Send the data to the etb_socket. */
    do {
#if DEBUG
        LOGMSG1("%s:sending from buf at 0x%x, %d bytes", __func__, 
                (uint32_t)send_buf, bytes_to_send);
#endif
        
        retry = socket_send_data(etb_socket, send_buf, bytes_to_send, 
                                 0, &bytes_sent);
        
        if (retry) {
           /* Reschedule what's left */
           etb_queue_element->buf_ptr = (uint32_t *)send_buf; 
           etb_queue_element->byte_cnt = bytes_to_send;
           return;
        }

        send_buf += bytes_sent;
        bytes_to_send -= bytes_sent;        
#if DEBUG
        LOGMSG1("%s: bytes actually sent %d, bytes remaining %d", __func__,
                bytes_sent, bytes_to_send);
#endif
    } while(bytes_to_send > 0);

    /* Update the queue head */    
    etb_queue_head = etb_queue_head->next; 
    free(etb_queue_element->buf_start);
    free(etb_queue_element);

    /* if queue is empty then remove socket from list. */
    if (etb_queue_head == NULL) {
        socket_remove_list(etb_socket);    
    }

}

/***************************************************************************** 
 *  Private functions
 *  
 *****************************************************************************/
/***************************************************************************** 
 *  etb_data_available()
 *
 *  - If not TEST_MODE then get the number of words available from ETB_status()
 *    and determine if the ETB has wrapped.
 *  - If TEST_MODE then get a random number of words.
 * 
 *****************************************************************************/
static int etb_data_available(bool * wrapped)
{
    ETBStatus etb_status;

    LOGFUNC();

    if (global_state.recording == STATE_UNINITIALIZED) {
        LOGMSG1("%s:Recording state is STATE_UNINITIALIZED so returning 0",
                __func__);
        return 0;
    }

#ifndef TEST_MODE
    {
        eETB_Error  etb_ret;
        etb_ret = ETB_status(etb_handle, &etb_status);
        if (etb_ret != eETB_Success) {

            LOGMSG1("%s:ETB_status error %d", __func__, etb_ret);
            err_handler(ERR_TYPE_LOCAL, ERR_ETB_ISSUE);
        }
        LOGMSG1("%s:etb status is:", __func__);
        LOGMSG1("%s: canRead              %d", __func__, etb_status.canRead);
        LOGMSG1("%s: isWrapped            %d", __func__, etb_status.isWrapped);
        LOGMSG1("%s: availableWords        %d", __func__, etb_status.availableWords);
        LOGMSG1("%s: ETB_TraceCaptureEn 0x%x", __func__, etb_status.ETB_TraceCaptureEn);
        LOGMSG1("%s: overflow           0x%x", __func__, etb_status.overflow);
    }

    /* ETB wrapped status is only valid while the etb is enabled. Once the ETB 
     * is disabled and any data read the wrapped status is not valid from ETBLib.
     */
    if (etb_is_enabled) {
        *wrapped = (etb_status.isWrapped) ? true : false;
    }  else {
        *wrapped = etb_disabled_wrapped;
    }

    if ((global_state.etb_mode == ETB_MODE_DRAIN) && (etb_status.overflow)) {

        LOGMSG1("%s:ETB_status call detected a ETB buffer overflow\n", __func__);
//TODO An overflow error should not kill the server - should
// respond with zero data available, and if called from status_command_handler
// it needs to return "overflow" as the state so the client knows an overflow occurred
// and it will not get any more data. An overflow also needs to terminate the current 
// recording session.
        err_handler(ERR_TYPE_LOCAL, ERR_ETB_OVERFLOW);
        
    }

#else /* Test mode - simulate etb data */


    if (global_state.recording == STATE_RECORDING) {

        /* Get number of bytes available to read */
        int size = rand();
        if ( (size < 0) || (size > 16384) ) {
            size &= 0x3FFF;
        };
        /* Round down to the nearest ETB word size in bytes */
        size -= size % etb_word_size;
        if ((size + test_fp_recpos) > test_fp_maxpos) {
            size = test_fp_maxpos - test_fp_recpos;
        }
        test_fp_recpos += size;
        etb_status.availableWords = size/etb_word_size;
    }
   
    switch (global_state.etb_mode) {
    case ETB_MODE_ONESHOT_FIXED:
    case ETB_MODE_ONESHOT_CIRCULAR:
    {
        if (global_state.recording == STATE_RECORDING) {
            etb_status.availableWords = test_fp_recpos/etb_word_size;
        } else {
            /* Must be STATE_STOPPED */
            if ((test_fp_recpos > 0) && (test_fp_dtat2send == 0)){
               test_fp_dtat2send = test_fp_maxpos;
               etb_status.availableWords = test_fp_maxpos/etb_word_size;
               test_fp_recpos = 0;
            }
            /* No data recorded and no data to read */
            if ((test_fp_recpos == 0) && (test_fp_dtat2send == 0)){
                etb_status.availableWords = 0;
            }
            /* Recorded data being read */  
            if ((test_fp_recpos == 0) && (test_fp_dtat2send > 0)){
                etb_status.availableWords = test_fp_dtat2send/etb_word_size;
            }          
        }       

        *wrapped = true;        
    
        LOGMSG1("%s:Oneshot mode - found %d etb words available",__func__,
                etb_status.availableWords);       
        break;
    }
    case ETB_MODE_DRAIN:
    {
        if (global_state.recording == STATE_STOPPED) {
            etb_status.availableWords = (test_fp_maxpos - test_fp_recpos)/etb_word_size;
            test_fp_recpos = 0;
        }

        *wrapped = true;

        LOGMSG1("%s:Drain mode - found %d etb words available",__func__,
                etb_status.availableWords);

        break;
    }
    } /* End of switch */
   
#endif

    LOGMSG1("%s:etb words available %d", __func__, etb_status.availableWords);
    return etb_status.availableWords;

}

/***************************************************************************** 
 *  etb_process_data()
 *
 *  This function Reads the ETB data and puts it into a buffer. The buffer is
 *  saved in a link list (etb_queue_t). 
 * 
 *  - Malloc a queue element.
 *  - Malloc a buffer to hold the ETB data.
 *  - If not TEST_MODE Read the ETB Data into the buffer.
 *  - If TEST_MODE then read the ETB data from the binary file.
 *  - Coverted the ETB Data to network endianess.
 *  - Add the buffer to the queue.
 *  - Since data is queued add the etb_socket to the list of 
 *    write sockets to test for ready.
 *****************************************************************************/
static void etb_process_data(int etb_word_cnt)
{
    LOGFUNC();    
    
    eETB_Error  etb_ret = eETB_Success;
    uint32_t read_size;
    struct etb_queue_t * etb_queue_element;

    /* Malloc a queue element. */
    etb_queue_element = malloc(sizeof(struct etb_queue_t));
    if  (etb_queue_element == NULL) {
        err_handler(ERR_TYPE_LOCAL, ERR_MEM_ALLOC);
    }

    /* Malloc a buffer to hold the ETB data. */
    etb_queue_element->byte_cnt = etb_word_cnt * sizeof(uint32_t);
    if (etb_queue_element->byte_cnt == 0) {
        LOGMSG1("%s:No ETB data available");
        return;
    }
    
    etb_queue_element->buf_start = malloc(etb_queue_element->byte_cnt);
    if  (etb_queue_element->buf_start == NULL) {
        free(etb_queue_element);
        err_handler(ERR_TYPE_LOCAL, ERR_MEM_ALLOC);
    }
    etb_queue_element->buf_ptr = etb_queue_element->buf_start;

    /* Read the ETB Data into the buffer. */
#ifndef TEST_MODE    
    etb_ret = ETB_read(etb_handle, etb_queue_element->buf_start, etb_word_cnt, 0,
                       etb_word_cnt, &read_size);
#else
    read_size = fread(etb_queue_element->buf_start, sizeof(uint32_t),
                      (size_t)etb_word_cnt, test_fp);
    
    LOGMSG1("%s:Test Mode, requested %d words, read %d words", __func__, 
            etb_word_cnt, read_size);
    if ((read_size < etb_word_cnt) && (ferror(test_fp))) {
        etb_ret = eETB_Error_Cannot_Read;

    }

    test_fp_dtat2send -= etb_word_cnt * sizeof(uint32_t);      
#endif
    if(etb_ret != eETB_Success) {

        LOGMSG1("%s:ETB_read error %d", __func__, etb_ret);
        err_handler(ERR_TYPE_LOCAL, ERR_ETB_ISSUE);
    }

    /* The ETB data is a buffer of 32-bit values. As such
     * it must be converted to network endianess.
     */
    {
        uint32_t * pdata_etb = etb_queue_element->buf_start;
        for (int i = 0; i < read_size; i++) {            
            *pdata_etb = htonl(*pdata_etb);
            pdata_etb++;
        }
    }

    /* Add the buffer to the queue. */
    LOGMSG1("%s:queuing buf at 0x%x, %d bytes", __func__,
        (uint32_t)etb_queue_element->buf_start, etb_queue_element->byte_cnt);
     
    etb_queue_element->next = NULL;
    if (etb_queue_head == NULL) {
        etb_queue_head = etb_queue_element;
        etb_queue_tail = etb_queue_element;
    } else {
        etb_queue_tail->next = etb_queue_element;
        etb_queue_tail = etb_queue_element;
    }

    etb_queue_cnt++;
    etb_words_queued += etb_word_cnt;

    /* Since data is queued add the etb_socket to the list of 
     * write sockets to test for ready.
     */
    socket_add_list(etb_socket, false);   /* 2nd argument is_read */
    
}


