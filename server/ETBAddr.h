/****************************************************************************
CToolsLib - ETB Library 

Copyright (c) 2009-2012 Texas Instruments Inc. (www.ti.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
****************************************************************************/
#ifndef __ETB_ADDR_H
#define __ETB_ADDR_H

#ifdef __cplusplus
extern "C" {
#endif

/*! \file CSETBAddr.h
    \version 1.2

    This file contains the ETB and TI Data Trace Formatter addresses.
    The "n" parameter used for the address is a CPU enumeration for homogeneous multi-core devices.
    For example, TCI6488 has n values as 0,1,3 for the three DSPs. 
    If a device does not have such multiple cores and associated ETBs, n is unused and should be 0.
    where ETB is being programmed and accessed.
 */

/************************************************************************** 
   Device specific information
   A new device can be supported by adding a device preprocessor 
   block specifying base addresses. 
**************************************************************************/

#if defined(TCI6612) || defined(TCI6614) || defined(TCI6616) || defined(TCI6618) \
|| defined(C6657) || defined(C6670) || defined(C6671) || defined(C6672) || defined(C6674) || defined(C6678) || defined(C66AK2Hxx) || defined(C66AK2Exx) && !defined(C66x)

#define C66x

#endif

#if defined(TCI6486)  
    #define NUM_ETB_INSTANCES 6
    #define DTF_PRESENT 1
    #define SYSETB_PRESENT 0
    
    /* ETB base address for different device types */
    #define _ETB_BaseAddress(n) ( 0x02C40000 + (n<<12) ) /* Base Address for ETB MMRs associated with GEMx */
    /* DTF MMR address. */
    #define DTF_CNTL(n)         ( 0x02A80100 + (n << 4)) /* memory mapped address for DTF Control register for GEMx */
   
#elif defined(TCI6488) 
    #define NUM_ETB_INSTANCES 3
    #define DTF_PRESENT 1
    #define SYSETB_PRESENT 0

    /* ETB base address for different device types */
    #define _ETB_BaseAddress(n) ( 0x02AD0000 + (n<<15) ) /* Base Address for ETB MMRs associated with GEMx */
    /* DTF MMR address. */
    #define DTF_CNTL(n)         (0x02880400 + (n << 2)) /* memory mapped address for DTF Control register for GEMx */
    
#elif defined(TCI6484) 
    #define NUM_ETB_INSTANCES 1
    #define DTF_PRESENT 1
    #define SYSETB_PRESENT 0

    /* ETB base address for different device types */
    #define _ETB_BaseAddress(n) (0x02AD0000) /* Base Address for ETB MMRs associated with GEMx */
    /* DTF MMR address */
    #define DTF_BASE(n)         (0x02AD1000) 
    #define DTF_CNTL(n)         (DTF_BASE(n) + 0x000) /* memory mapped address for DTF Control register for GEMx */
    #define TAGSET(n)           (DTF_BASE(n) + 0xFA0) /* memory mapped address for DTF Control register for GEMx */
    #define TAGCLR(n)           (DTF_BASE(n) + 0xFA4) /* memory mapped address for DTF Control register for GEMx */
    #define DTF_LOCK(n)         (DTF_BASE(n) + 0xFB0) /* memory mapped address for DTF Control register for GEMx */
    #define DTF_LOCK_STATUS(n)  (DTF_BASE(n) + 0xFB4) /* memory mapped address for DTF Control register for GEMx */
    #define ID(n)               (DTF_BASE(n) + 0xFC8) /* memory mapped address for DTF Control register for GEMx */
    /* PSC MMR address */
    #define PSC_BASE            (0x02AC0000) 
    #define PSC_MDCTL(n)        (PSC_BASE+ 0xA00 + (4*n)) /* memory mapped address for Module Control Register for clock, reset and EMU behavior control */
    #define PSC_MDSTAT(n)       (PSC_BASE+ 0x800 + (4*n)) /* memory mapped address for Module status register */ 
    #define PSC_PTCMD           (PSC_BASE+ 0x120) /* memory mapped address for transition command register */
    #define PSC_PTSTAT          (PSC_BASE+ 0x128) /* memory mapped address for transition command status register */ 

#elif defined(C66x)
    #define NUM_ETB_INSTANCES   9 /* For 6616, there are 4 DSP ETB and 1 SYS ETB. For 6678/6608, there are 8 DSP ETB and 1 SYS ETB */
    #define SYSETB_PRESENT 1
    #define SYS_ETB_ID  8 /* calculated to get system etb base address using _ETB_BaseAddress(n). For keystone2 devices, SYS_ETB_ID (TBR)
                             is not used to get system etb base address using _ETB_BaseAddress(n).  */
#ifndef __linux
    #define DTF_PRESENT 1
    #define DMA_SUPPORT
#endif

#if defined(C66AK2Hxx) || defined(C66AK2Exx)
    /* Get ETB base address for different cores and system ETB */
    #define _ETB_BaseAddress(n) ((n==SYS_ETB_ID)?(0x03019000):(0x027D0000 + (n << 16))) /* Base Address for ETB MMRs associated with CorePACx and SYS ETB (TBR) */
#else
#ifdef __linux
    #define ETB_BaseAddress(n) (0x027D0000 + (n << 16))
    #define _ETB_BaseAddress(n) virtural_ETB_BaseAddress[n]
    #define SIZEOF_ETB_SPACE 4096
#else
    /* Get ETB base address for different cores and system ETB */
    #define _ETB_BaseAddress(n) (0x027D0000 + (n << 16)) /* Base Address for ETB MMRs associated with CorePACx and SYS ETB*/
#endif
#endif

    /* Debug SS MIPI STM TBR DMA slave port address */
    #define TBR_RBD          (0x02850000)
    
    /* DTF MMR address */
#ifdef __linux
    #define DTF_BaseAddress(n)  (0x02440000 + (n << 16))
    #define DTF_BASE(n)         virtural_DTF_BaseAddress[n]
    #define SIZEOF_DTF_SPACE    4096
#else
    #define DTF_BASE(n)         (0x02440000 + (n << 16))
#endif
    #define DTF_CNTL(n)         (DTF_BASE(n) + 0x000) /* memory mapped address for DTF Control register for C66x */
    #define TAGSET(n)           (DTF_BASE(n) + 0xFA0) /* memory mapped address for DTF Control register for C66x */
    #define TAGCLR(n)           (DTF_BASE(n) + 0xFA4) /* memory mapped address for DTF Control register for C66x */
    #define DTF_LOCK(n)         (DTF_BASE(n) + 0xFB0) /* memory mapped address for DTF Control register for C66x */
    #define DTF_LOCK_STATUS(n)  (DTF_BASE(n) + 0xFB4) /* memory mapped address for DTF Control register for C66x */
    #define ID(n)               (DTF_BASE(n) + 0xFC8) /* memory mapped address for DTF Control register for C66x */
    
    /* PSC MMR address */
#ifdef __linux
    #define PSC_BaseAddress     (0x02350000)
    #define PSC_BASE            virtural_PSC_BaseAddress
    #define SIZEOF_PSC_SPACE    4096
#else
    #define PSC_BASE            (0x02350000)
#endif
#define PSC_PDSTAT(n)  (PSC_BASE + 0x200 + (4*n))
    #define PSC_PDCTL(n)        (PSC_BASE + 0x300 + (4*n)) /* memory mapped address for Power Domain Control Register */     
    #define PSC_MDCTL(n)        (PSC_BASE + 0xA00 + (4*n)) /* memory mapped address for Module Control Register for clock, reset and EMU behavior control */
    #define PSC_MDSTAT(n)       (PSC_BASE+ 0x800 + (4*n)) /* memory mapped address for Module status register */ 
    #define PSC_PTCMD           (PSC_BASE + 0x120) /* memory mapped address for transition command register */
    #define PSC_PTSTAT          (PSC_BASE + 0x128)  /* memory mapped address for transition command status register */ 

#elif defined(TI816x) 
    #define NUM_ETB_INSTANCES   1
    #define SYSETB_PRESENT 1
    #define SYS_ETB_ID  1 

    /* ETB base address for different device types */
    // the variable 'n' is used as a dummy and always 'zero' is added to the base address. This is done to remove a compiler warning
    #define _ETB_BaseAddress(n) ( 0x4B162000 + (0 << n) ) /* Base Address for ETB MMRs associated with TI81x - one ETB*/
    
    #if defined(ETM) 
        #define DTF_PRESENT 0
    #elif defined(DSP) 
        #define DTF_PRESENT 1
    #elif defined(STM) 
        #define DTF_PRESENT 0
    #endif

    /* DTF MMR address */
    #define DTF_BASE(n)         (0x4B166000 + (n << 16)) 
    #define DTF_CNTL(n)         (DTF_BASE(n) + 0x000) /* memory mapped address for DTF Control register for C66x */
    #define TAGSET(n)           (DTF_BASE(n) + 0xFA0) /* memory mapped address for DTF Control register for C66x */
    #define TAGCLR(n)           (DTF_BASE(n) + 0xFA4) /* memory mapped address for DTF Control register for C66x */
    #define DTF_LOCK(n)         (DTF_BASE(n) + 0xFB0) /* memory mapped address for DTF Control register for C66x */
    #define DTF_LOCK_STATUS(n)  (DTF_BASE(n) + 0xFB4) /* memory mapped address for DTF Control register for C66x */
    #define ID(n)               (DTF_BASE(n) + 0xFC8) /* memory mapped address for DTF Control register for C66x */

#elif defined(OMAP3x)//OMAP3
    #define DTF_PRESENT 0
    #error Need to find out ETB base address from device data sheet for this device.

#elif defined(_OMAP) || defined(_OMAP54xx)
    #define NUM_ETB_INSTANCES   1
    #define SYSETB_PRESENT      1
    #define SYS_ETB_ID          1 

#ifdef _OMAP
    /* ETB base address for OMAP devices */
    // the variable 'n' is used as a dummy and always 'zero' is added to the base address. This is done to remove a compiler warning
    #define _ETB_BaseAddress(n) ( 0x54162000 + (0 << n) ) /* Base Address for ETB MMRs - one ETB */
#endif

#ifdef _OMAP54xx
	/* TBR base address for OMAP54xx ES2 devices */
    /* Note that for OMAP5 ES1 the OMAP4430 A9 build will work - _OMAP */
    #define _ETB_BaseAddress(n) ( 0x54167000 ) /* Base Address for ETB MMRs - one ETB */
#endif

    #if defined(ETM) 
        #define DTF_PRESENT 0
    #elif defined(DSP) 
        #define DTF_PRESENT 1
    #elif defined(STM) 
        #define DTF_PRESENT 0
    #endif

#ifdef _OMAP54xx
    #define ENABLE_ETB_FORMATTER
#endif
    /* DTF (Trace Funnel) MMR address, Registers defined in CoreSight Components TRM */
    #define DTF_BASE(n)         (0x54164000 + (n << 16)) 
    #define DTF_CNTL(n)         (DTF_BASE(n) + 0x000) /* Funnel control register */
    #define TAGSET(n)           (DTF_BASE(n) + 0xFA0) /* Claim Tag Set register  */
    #define TAGCLR(n)           (DTF_BASE(n) + 0xFA4) /* Claim Tag Clear register */
    #define DTF_LOCK(n)         (DTF_BASE(n) + 0xFB0) /* Lock Access - WO */
    #define DTF_LOCK_STATUS(n)  (DTF_BASE(n) + 0xFB4) /* Lock Status - RO */
    #define ID(n)               (DTF_BASE(n) + 0xFC8) /* Device ID */

#elif defined(TCI6612_CSETB) || defined(TCI6614_CSETB) 
    #define NUM_ETB_INSTANCES   1
    #define SYSETB_PRESENT      1
    #define SYS_ETB_ID          1 
    #define DTF_PRESENT 0

    /* ETB base address for TCI6614 device */
    // the variable 'n' is used as a dummy and always 'zero' is added to the base address. This is done to remove a compiler warning
    #define _ETB_BaseAddress(n) ( 0x025A6000 + (0 << n) ) /* Base Address for CS-ETB */

#elif defined(C66AK2Hxx_CSSTM_ETB) || defined(C66AK2Exx_CSSTM_ETB)

    #define NUM_ETB_INSTANCES   1
    #define SYSETB_PRESENT      1
    #define SYS_ETB_ID          1
    #define DTF_PRESENT 0
    #define DMA_SUPPORT

    /* CSSTM ETB base address for Keystone2 devices */
    #define _ETB_BaseAddress(n) (0x03020000 + (n << 12)) /* Base Address for CSSTM-ETB, 'n' is the A15 MPU SS id. For keystone2, there is only one A15 MPU SS and n=0  */

    /* CSSTM TBR DMA slave port address */
    #define TBR_RBD          (0x027D4000)

#if defined(C66AK2Hxx_CSSTM_ETB)
#define C66AK2Hxx
#elif defined(C66AK2Exx_CSSTM_ETB)
#define C66AK2Exx
#endif

#elif defined(C66AK2Hxx_CSSTM_ETB) || defined(C66AK2Exx_CSSTM_ETB)

    #define NUM_ETB_INSTANCES   1
    #define SYSETB_PRESENT      1
    #define SYS_ETB_ID          1
    #define DTF_PRESENT 0

    /* CSSTM ETB base address for Keystone2 devices */
    #define _ETB_BaseAddress(n) (0x03020000 + (n << 12)) /* Base Address for CSSTM-ETB, 'n' is the A15 MPU SS id. For keystone2, there is only one A15 MPU SS and n=0  */

    /* CSSTM TBR DMA slave port address */
    #define TBR_RBD          (0x027D4000)

#if defined(C66AK2Hxx_CSSTM_ETB)
#define C66AK2Hxx
#elif defined(C66AK2Exx_CSSTM_ETB)
#define C66AK2Exx
#endif

#else
    #error No device type preprocessor defined for the ETBLib
#endif




/************************************************************************** 
   No changes need to be made below this point to support a new device 
**************************************************************************/

/* Registers common for both TI-ETB and TBR implementations*/

/* ETB RAM Depth Register RDP */
/* TBR RAM Size Register  */
#define ETB_RDP(n)          (_ETB_BaseAddress(n) + 0x004)
/* ETB Status Register STS */
/* TBR Status Register */
#define ETB_STS(n)          (_ETB_BaseAddress(n) + 0x00C)
/* ETB/TBR RAM Read Data Register RRD */
#define ETB_RRD(n)          (_ETB_BaseAddress(n) + 0x010)
/* ETB/TBR RAM Read Pointer Register RRP */
#define ETB_RRP(n)          (_ETB_BaseAddress(n) + 0x014)
/* ETB/TBR RAM Write Pointer Register RWP */
#define ETB_RWP(n)          (_ETB_BaseAddress(n) + 0x018)
/* ETB/TBR Trigger counter register */
#define ETB_TRIG(n)         (_ETB_BaseAddress(n) + 0x01C)
/* ETB/TBR Control Register CTL */
#define ETB_CTL(n)          (_ETB_BaseAddress(n) + 0x020)
/* ETB/TBR RAM Write Data Register RWD */
#define ETB_RWD(n)          (_ETB_BaseAddressn(n)+ 0x024)
/* ETB Formatter and Flush Status Register FFSR */
/* TBR Operation Status Register OPSTAT */
#define ETB_FFSR(n)         (_ETB_BaseAddress(n) + 0x300)
/* ETB Formatter and Flush Control Register FFCR */
/* TBR Operations Control Register OPCTRL*/
#define ETB_FFCR(n)         (_ETB_BaseAddress(n) + 0x304)
/* ETB/TBR Lock Access Register */
#define ETB_LOCK(n)         (_ETB_BaseAddress(n) + 0xFB0)
/* ETB/TBR Lock Status Register */
#define ETB_LOCK_STATUS(n)  (_ETB_BaseAddress(n) + 0xFB4)
/* ETB/TBR device ID Register */
#define ETB_DEVID(n)        (_ETB_BaseAddress(n) + 0xFC8)

/* Registers specific to TI-ETB implementation*/
#define ETB_WIDTH(n)        (_ETB_BaseAddress(n) + 0x008) /* ETB RAM Width Register STS */
#define ETB_RBD(n)          (_ETB_BaseAddress(n) + 0xA00) /* ETB RAM burst read Register */
#define ETB_TI_CTL(n)       (_ETB_BaseAddress(n) + 0xE20) /* ETB TI Control Register */
#define ETB_IRST(n)         (_ETB_BaseAddress(n) + 0xE00) /* ETB TI Interrupt Raw Status Register */
#define ETB_ICST(n)         (_ETB_BaseAddress(n) + 0xE04) /* ETB TI Interrupt Raw Status Register */
#define ETB_IER(n)          (_ETB_BaseAddress(n) + 0xE0C) /* ETB TI Interrupt Enable Register */
#define ETB_IECST(n)        (_ETB_BaseAddress(n) + 0xE10) /* Clear interrupt enable bits */

/* Registers specific to TBR implementation */
#define TBR_FIFOSZ(n)          (_ETB_BaseAddress(n) + 0x008) /*TBR Output FIFO Size Register */
#define TBR_OUTLVL(n)          (_ETB_BaseAddress(n) + 0x100) /*Output FIFO Trigger Level Register */
#define TBR_SICTRL(n)          (_ETB_BaseAddress(n) + 0x104) /*TBR System Interface Control */
#define TBR_IDPERIOD(n)        (_ETB_BaseAddress(n) + 0x108) /*ID Repeat Period Register */
#define TBR_SEQCNTL(n)         (_ETB_BaseAddress(n) + 0x10C) /*Message Sequence Insertion Control */
#define TBR_EOI(n)             (_ETB_BaseAddress(n) + 0x120) /*TBR EOI register */
#define TBR_IRQSTATUS_RAW(n)   (_ETB_BaseAddress(n) + 0x124) /*TBR IRQ Status (Raw) register */
#define TBR_IRQSTATUS(n)       (_ETB_BaseAddress(n) + 0x128) /*TBR IRQ Status register */
#define TBR_IRQENABLE_SET(n)   (_ETB_BaseAddress(n) + 0x12C) /*TBR IRQ Enable set register */
#define TBR_IRQENABLE_CLR(n)   (_ETB_BaseAddress(n) + 0x130) /*TBR IRQ Enable clear register */
#define TBR_CLAIMSET(n)        (_ETB_BaseAddress(n) + 0xFA0) /*Claim Tag Set Register */
#define TBR_CLAIMCLR(n)        (_ETB_BaseAddress(n) + 0xFA4) /*Claim Tag Clear Register */
#define TBR_AUTHSTAT(n)        (_ETB_BaseAddress(n) + 0xFB8) /*Authorization Status Register */


/* ETB enable bit */
#define ETB_ENABLE      0x00000001
/* ETB Status Register Bit definitions */
#define ETB_STS_ACQCOMP  0x00000004 /* bit 2: 1=acquisition complete */
#define ETB_STS_FULL     0x00000001 /* bit 0: 1=RAM full */
/* ETB Formatter and Flush Status Register bits */
#define ETB_FLUSH_INPROGRESS 0x00000001 /* bit 0: 1 = flush in progress */
/* ETB unlock value */
#define ETB_UNLOCK_VAL   0xC5ACCE55 /* Value to unlock ETB for register accesses */

/* DTF values */
#define LOCK_STATUS_IMP_BIT		(1<<0)
#define LOCK_STATUS_STAT_BIT	(1<<1)
#define DTF_ID_MAJOR_MASK	    (0xF<<4)	   
#define DTF_ID_MAJOR_VER1	    (0x1<<4)	   
#define DTF_ID_MAJOR_VER2	    (0x2<<4)	
#define TI_ETB_CIRCULARMODE_BIT	(0x1<<1)
#define TI_ETB_TI_MODE          (0x1<<0)

#define TBR_BRIDGE_MODE         (0x1<<1)
#define TBR_BUFFER_MODE         (0xFFFFFFFD)

#define DTF_VER2_FLUSH_BIT  (1<<7)

#define TI_ETB_IRST_UNDERFLOW (1 << 3)
#define TI_ETB_IRST_OVERFLOW  (1 << 2)
#define TI_ETB_IRST_FULL      (1 << 1)
#define TI_ETB_IRST_HALF_FULL (1 << 0)

#define TBR_IRST_AQCMP      (1 << 1)
#define TBR_IRST_DAV        (1 << 0)
#define TBR_STP_FULL        (1 << 15)

#define TBR_TWP_DISABLE (0xFFFFFFFE)
#define TBR_TWP_ENABLE  (0x1)

#define TBR_TWP_IDPERIOD (0x8)

#define TBR_TWP_SEQATBID (0x6F << 16)
#define TBR_TWP_SEQPERIOD (0x10)

/* TBR Operations Control Register (OPCTRL)bits */
#define TBR_OUTFLUSH_INPROGRESS (1<<16) /* bit 16: 1 = output flush in progress */
#define TBR_OUTFLUSH_START (1<<16) /* set bit 16 = 1 : output flush in started */

/* TBR Status Register(STAT) bits */
#define TBR_DRAIN_INPROGRESS (1<<4) /* bit 4: 1 = DMA drain in progress */

/* TBR System interface control (SICTRL) register */
#define TBR_READ_REQ_PENDING (1<<1)

#if defined(C66AK2Hxx_CSSTM_ETB) || defined(C66AK2Exx_CSSTM_ETB)

#define TBR_NUMBLOCK (0xF)
#define TBR_BLOCKSZ (0x1F)

#else

#define TBR_NUMBLOCK (0xF)
#define TBR_BLOCKSZ (0x3F)

#endif

/* TI-ETB and TBR device identifiers */
#define ETB_DEVICE_ID (0x20)
#define TBR_DEVICE_ID (0x11)


/* ETB Maximum Burst Size value */
#define ETB_BURST_SIZE  0x400

#ifdef __cplusplus
}
#endif

#endif //__ETB_ADDR_H
