/*
 * command_handler.c
 *
 * Ctools Profiler Server Implementation
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include "error_handler.h"
#include "logging.h"
#include "socket.h"
#include "remote_commands.h"

/***************************************************************************** 
 *  Internal Function Prototypes 
 *  - See Private Function section below for implementations )
 *****************************************************************************/
static inline uint8_t calculate_checksum( uint8_t *buf, unsigned int len );
static inline uint8_t int_to_hexdigit(unsigned val);
static inline void send_response(uint8_t * response);
static void command_dispatch(char * cmd, int cmd_len, char * response, size_t rsp_size);
static void command_clr_buf(uint8_t * pbuf);

static void get_memory_arguments(char * cbuf, uint32_t * addr, size_t * len, char ** data);
static void process_memory_write(char * cbuf, char * rbuf, size_t rsp_size);
static void process_memory_read(char * cbuf, char * rbuf, size_t rsp_size);
static void process_remote(char * cbuf, char * rbuf, size_t rsp_size);
static void process_disable_ack(char * cbuf, char * rbuf, size_t rsp_size);

/***************************************************************************** 
 *  Remote Serial Protocol (RSP) Definitions 
 *  - RSP commands consists of remote commands and target commands (memory read 
 *    and memory write.
 *  
 *****************************************************************************/

/* RSP Commands */
typedef enum {
    READ_MEMORY_CMD,
    WRITE_MEMORY_CMD,
    REMOTE_CMD,
    DISABLE_ACK_CMD
} rsp_cmd_t;

typedef void (* proc_func_t)(char * cbuf, char * rbuf, size_t rbuf_size);

struct rsp_command_table_t {
    char * cmd;
    proc_func_t process;
};

static struct rsp_command_table_t rsp_command_table[] = {
    [READ_MEMORY_CMD] = {"m", process_memory_read},
    [WRITE_MEMORY_CMD] = {"M", process_memory_write},
    [REMOTE_CMD] = {"qRcmd,", process_remote},
    [DISABLE_ACK_CMD] = {"QStartNoAckMode", process_disable_ack}
};

/* RSP remote command table */
typedef void (* rmt_func_t)(int argc, char *argv[], char * rbuf, size_t rsp_size);

struct remote_command_table_t {
    char * cmd;
    rmt_func_t process;
};

/* The commands from this table are provided from remote_commands.h */
static struct remote_command_table_t remote_command_table[] = {
    {"set", set_command_handler},
    {"mmap", mmap_command_handler},
    {"ummap", ummap_command_handler},
    {"version", version_command_handler},
    {"arm", modcntl_add_command_handler},
    {"disarm", modcntl_add_command_handler},
    {"rm_arm", modcntl_remove_command_handler},
    {"rm_disarm", modcntl_remove_command_handler},
    {"start", start_command_handler},
    {"end", end_command_handler},
    {"status", status_command_handler}
};

/* RSP constants */
static bool rsp_ack_enabled = true;
static const char *ACK  = "+";
static const char *NACK  = "-";
static const char *rsp_msg_ok = "OK";

/* RSP State */
typedef enum {AWAITING_COMMAND, AWAITING_ACK} command_state_t;
static command_state_t command_state = AWAITING_COMMAND;

/* RSP buffers*/
#define MAX_COMMAND_SIZE 256
static uint8_t cmd_buf[MAX_COMMAND_SIZE];
static uint8_t rsp_buf[MAX_COMMAND_SIZE];

/***************************************************************************** 
 *  Statics 
 *  
 *****************************************************************************/
static int cmd_socket;

/***************************************************************************** 
 *  Public Functions
 *  
 *****************************************************************************/
/***************************************************************************** 
 *  command_init()
 *
 *  Initialize the client command handler with a socket.
 *  
 *****************************************************************************/
void command_init(int socket)
{
    LOGFUNC();
    cmd_socket = socket;
    command_clr_buf(cmd_buf);

    command_state = AWAITING_COMMAND;
    rsp_ack_enabled = true;
}

/***************************************************************************** 
 *  command_process()
 *
 *  This is where all client commands are received and executed.
 *
 *  - RSP packet definition is:
 *      $packet-data#checksum
 *
 *  Where checksum is a 2 digit module 256 sum (in hex) of the packet-data.
 *  Initial response from the server is “+/-“ where “+” indicates the packet-data
 *  transmitted successfully, and “-“ indicates the packet-data needs to be resent.
 *  All values are sent in HEX format without leading “0x”.
 *
 *  Examples:
 *    $Maddr,length:data#checksum - Write length bytes of memory starting at addr.
 *    $maddr,length#checksum - Read length bytes of memory starting at addr.  
 *    $qRcmd,mmap addr,size#checksum  - Map memory block at addr of size in bytes.
 * 
 *****************************************************************************/ 
void command_process()
{
    uint8_t * pbuf = cmd_buf;
    uint8_t * cksum_marker;
    int retry;
    size_t cmd_len;

    LOGFUNC();

    /* Provide size of receive buffer as MAX_COMMAND_SIZE -1
     * because we want room for at least one NULL character in the buffer. 
     */ 
    retry = socket_recv_data(cmd_socket, cmd_buf, MAX_COMMAND_SIZE - 1, 0, 
                             &cmd_len);

    if ((retry) || (cmd_len == 0)) {
        return;
    }

    LOGMSG1("%s:Received msg length %d, message is:%s, command state is %d",
            __func__, cmd_len, cmd_buf, command_state);

    /* Note: Since the server is non-blocking the ACK (if AWAITING_ACK set) 
     *  and the next command can be combined into a single message.
     */
    if (command_state == AWAITING_ACK) {
        if (cmd_buf[0] == '+') {
            command_state = AWAITING_COMMAND;
            LOGMSG1("%s:Processed ACK", __func__);

            if (cmd_len > 1) {
                pbuf++;
            } else {
                command_clr_buf(cmd_buf);
                command_clr_buf(rsp_buf);
                return;
            }

        }
        if (cmd_buf[0] == '-') {
            send_response(rsp_buf);
            LOGMSG1("%s:Processed NACK", __func__);
            return;
        }
    }

    /* First character of RSP commands must be $ */
    if (('$' != pbuf[0]) || !( cksum_marker = (uint8_t *)strchr((char *)pbuf, '#'))) {
        err_handler(ERR_TYPE_LOCAL, ERR_RSP_INVALID);
    }

    /* get past first character and determine the actual command length */
    pbuf++;     
    cmd_len = cksum_marker - pbuf;

    /* Validate checksum */
    if (rsp_ack_enabled == true) {
        uint8_t ck_sum = calculate_checksum( pbuf, cmd_len );
        uint8_t c1 = pbuf[cmd_len+1];
        uint8_t c2 = pbuf[cmd_len+2];

        if ((c1 != int_to_hexdigit(ck_sum >> 4)) ||
            (c2 != int_to_hexdigit(ck_sum & 0xf ))) {
            /* Simply means resend command*/
            send_response((uint8_t *)NACK);
            return;
        }
        else {
            send_response((uint8_t *)ACK);
        }
        
    }

    /* Replace the # with 0 so it looks like the end of the string */
    *cksum_marker = 0;

    LOGMSG1("%s:Received command from client:%s", __func__, pbuf);

    /* Execute the command */
    {
        uint8_t * rsp_p = rsp_buf;
        uint8_t ck_sum = 0; 
        int rsp_len;

        command_clr_buf(rsp_buf);
        rsp_p[0] = '$';
        rsp_p++;          

        command_dispatch((char *)pbuf, cmd_len, (char *)rsp_p, sizeof(rsp_buf)-1);

        rsp_len = strlen((char *)rsp_p);
        
        if (rsp_ack_enabled == true) {
            ck_sum = calculate_checksum(rsp_p, rsp_len);
        }

        rsp_p[rsp_len] = '#';
        rsp_p[rsp_len+1] = int_to_hexdigit(ck_sum >> 4);
        rsp_p[rsp_len+2] = int_to_hexdigit(ck_sum & 0xf);
        
        send_response(rsp_buf);

        LOGMSG1("%s:Sent response to client:%s", __func__, rsp_buf);

        if (rsp_ack_enabled == true) {
            command_state = AWAITING_ACK;
        } else {
            command_clr_buf(cmd_buf);
        }
     } 

}

/***************************************************************************** 
 *  Private functions
 *  
 *****************************************************************************/
/***************************************************************************** 
 *  command_clr_buf()
 *
 * Fill the command buffer with the NULL character.
 * Note: This is done so strchr will not have problems if a non-conforming rsp
 *       command is received.
 *  
 *****************************************************************************/ 
void command_clr_buf(uint8_t * pbuf)
{
    memset((char *)pbuf, '\0', MAX_COMMAND_SIZE);
}

/***************************************************************************** 
 *  calculate_checksum()
 *
 *  Calculate the RSP checksum
 *  
 *****************************************************************************/ 
static inline uint8_t calculate_checksum( uint8_t *buf, unsigned int len ) 
{
    unsigned checksum = 0;
    while ( len-- > 0 ) {
        checksum += *buf++;
    }     
    return checksum & 0xFF;
}

/***************************************************************************** 
 *  int_to_hexdigit()
 *
 *  Convert a int digit to a hex ascii value.
 *  
 *****************************************************************************/ 
static const uint8_t inttohex_table[] = {
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' 
};

static inline uint8_t int_to_hexdigit(unsigned int val)
{
    if ( val > 0xF) {
        err_handler(ERR_TYPE_LOCAL, ERR_RSP_INVALID);
    }
    return inttohex_table[val];
}

/***************************************************************************** 
 *  send_response()
 *
 *  Once the command is processed this function is used to send the response
 *  back to the client.
 *  
 *****************************************************************************/
static inline void send_response(uint8_t * response)
{
    size_t send_offset = 0;
    size_t retlen;
    int retval;

    /* The command socket is non-blocking so must check for EAGAIN */
    do {
        retval =  socket_send_data(cmd_socket, response, 
                                   strlen((char *)response), send_offset, &retlen);
    } while(retval);
}

/***************************************************************************** 
 *  command_dispatch()
 *
 *  - Search the rsp_command_table for a known cmd that matches the received cmd
 *  - Use the command index to execute the rsp command handler.
 *  
 *****************************************************************************/
void command_dispatch(char * cmd, int cmd_len, char * response, size_t rsp_size)
{
    int rsp_cmd_elements = sizeof(rsp_command_table)/sizeof(struct rsp_command_table_t);
    int cmd_index;
    char * pcmd = cmd;

    LOGFUNC();

    /* Search the rsp_command_table for a known cmd that matches the received cmd. */
    for ( cmd_index = 0; cmd_index < rsp_cmd_elements; cmd_index++) {
        
        char *rsp_cmd = strstr(cmd, rsp_command_table[cmd_index].cmd);
        if (rsp_cmd == cmd) {
            pcmd += strlen(rsp_command_table[cmd_index].cmd);

            LOGMSG1("%s:RSP Command match found:%s", __func__, 
                    rsp_command_table[cmd_index].cmd);

            break;
        }
    }

    if (cmd_index == rsp_cmd_elements) {
        err_handler(ERR_TYPE_LOCAL, ERR_RSP_INVALID);
    }


    /* Process the command */
    rsp_command_table[cmd_index].process(pcmd, response, rsp_size);
}

/***************************************************************************** 
 *  get_memory_arguments()
 *
 *  Convert ascii memory command arguments (addr, len, and data) and into
 *  values.
 *
 *  Note - data is not converted to value in this function. It simply returns
 *         a char * to the data value if this is a write, else NULL is returned.
 *  
 *****************************************************************************/
static void get_memory_arguments(char * cbuf, uint32_t * addr, size_t * len, char ** data)
{
    char * tokins = ",:";
    /* arg[0] used to hold the char * to the address */
    /* arg[1] used to hold the char * to the length in bytes */
    /* arg[2] used to hold a char * to the data if this is a write */
    char * arg[3] = {NULL,NULL,NULL};
    char addr_str[] = "0x00000000";
    char len_str[] = "0x00000000";
    int starting_digit;

    int i = 0;
    arg[i] = strtok(cbuf, tokins);
    while (arg[i] != NULL) {        
#if DEBUG
        if ((strlen(arg[i]) == 0) || (strlen(arg[i]) > 8)) {
            err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
        }
#endif
        arg[++i] = strtok(NULL, tokins);
        /* Don't overflow the arg array */
        if (i == 2) break;
    }

#if DEBUG
    if ( (arg[0] == NULL) || (arg[1] == NULL)) {
        err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
    }

#endif

    starting_digit = 10 - strlen(arg[0]);
    strcpy(&addr_str[starting_digit], arg[0]);

    starting_digit = 10 - strlen(arg[1]);
    strcpy(&len_str[starting_digit], arg[1]);

    *addr = strtoul(addr_str, NULL, 16);
    *len = strtoul(len_str, NULL, 16);  /* in bytes */
    *data = arg[2];
    
}

/***************************************************************************** 
 *  process_memory_write()
 *
 *  RSP target memory write command.
 *
 *  Note - data is restricted to N 32-bit hex values.
 *  
 *****************************************************************************/
static void process_memory_write(char * cbuf, char * rbuf, size_t rsp_size)
{
    uint32_t addr;
    size_t len;
    char * data_str;

    get_memory_arguments(cbuf, &addr, &len, &data_str);
#if DEBUG
    if ((data_str == NULL) || (len % 4 != 0) || (addr & 0x3)) {
        err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
    }
#endif

    /* Client must send 8 hex digits per word */
    do {
        /* Do a 32-bit write */
        uint32_t data;
        int offset = 0;            
        char word[] = "0x00000000";   /* data_str is in byte order 78563412 for 0x12345678*/
        
        int num_digits = strlen(data_str + offset);
        if (num_digits > 8) {
            num_digits = 8;
        }

        for (int i = 0; i < num_digits; i += 2) {
            word[8 - i] = data_str[offset + i];
            word[9 - i] = data_str[offset + i + 1];   
        }

        data =  strtoul(word, NULL, 16); 

        remote_memory_write( addr, sizeof(uint32_t), &data);

        offset += 8;
        len -= 4;
 
    } while (len != 0);

#if DEBUG
    if (strlen(rsp_msg_ok) > rsp_size) {
        err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
    }
#endif
    strcpy(rbuf, rsp_msg_ok);

}

/***************************************************************************** 
 *  process_memory_read()
 *
 *  RSP target memory read command.
 *
 *  Note - data is restricted to N 32-bit hex values.
 *  
 *****************************************************************************/
static void process_memory_read(char * cbuf, char * rbuf, size_t rsp_size)
{
    uint32_t addr;
    size_t len;
    char * rsp = rbuf;
    char * data_str;
#if DEBUG 
    int digit_cnt = 0;
#endif

    get_memory_arguments(cbuf, &addr, &len, &data_str);

#if DEBUG
    /* data_str only valid for write, not read */
    if ((data_str != NULL) || (len % 4 != 0)  || (addr & 0x3)) {
        err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
    }
#endif

    /* Will send 8 hex digits per word */
    do {
        /* Do a 32-bit write */
        uint32_t data, adj_data, mask;
        int num_digits;

#if DEBUG        
        if (digit_cnt > rsp_size) {
            err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
        }
#endif

        remote_memory_read(addr, sizeof(uint32_t), &data);

        /* when len < 4 bytes suppress leading zeros */
        if (len > 4) {
            num_digits = 8;
        } else {
            if (data < 256) num_digits = 2;
            else if ((data > 255) && (data < 65536)) num_digits = 4;
            else if ((data > 65535) && ( data < 16777216)) num_digits = 6;
            else if (data > 16777215) num_digits = 8;
        }

        for (int i = 0; i < num_digits; i += 2) {
            adj_data = data >> (i * 4);
            mask = 0xf0;
            *rsp++ = int_to_hexdigit((adj_data & mask) >> 4);
            *rsp++ = int_to_hexdigit(adj_data & (mask >> 4));
        }

#if DEBUG 
        digit_cnt += num_digits;
#endif
        len -= sizeof(uint32_t);
 
    } while (len != 0);

}

/***************************************************************************** 
 *  process_remote()
 *
 *  Process remote RSP commands
 *
 *  - Search the remote_command_table for a known cmd that matches the received cmd.
 *  - Tokenize the remote command into argv argc pair.
 *  - Execute the command from the remote command table.
 *
 *****************************************************************************/
static void process_remote(char * cbuf, char * rbuf, size_t rsp_size)
{

    int remote_element_cnt = sizeof(remote_command_table)/sizeof(struct remote_command_table_t);
    int op_index;

    LOGFUNC();

    /* Search the remote_command_table for a known cmd that matches the received cmd */
    for ( op_index = 0; op_index < remote_element_cnt; op_index++) {
        /* Search the cmd string for the rsp command prefix */
        char *rsp_cmd = strstr(cbuf, remote_command_table[op_index].cmd);

        if (rsp_cmd == cbuf) {
            LOGMSG1("%s:Remote command match found:%s", __func__,
                    remote_command_table[op_index].cmd);
            break;
        }
    }

    if (op_index == remote_element_cnt) {
        err_handler(ERR_TYPE_LOCAL, ERR_RSP_INVALID);
    }

    /* Tokenize the remote command into argv argc pair. */
    {    
        #define MAX_REMOTE_ARGS 6
        char * argv[MAX_REMOTE_ARGS];
        int argc = 0;
        char * tokins = " ,"; /* Space and comma */
    
        argv[argc] = strtok(cbuf, tokins); 
        while (argv[argc]) {
            argc++;
#if DEBUG
            if ( argc == MAX_REMOTE_ARGS) {
                err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
            }
#endif
            argv[argc] = strtok(NULL, tokins);
        }

        /* Process the command */

        IF_LOGGING_ENABLED {
            for (int i = 0; i < argc; i++) {
                LOGMSG1("%s: argv[%d] is:%s", __func__, i, argv[i]);
            }
        }

        remote_command_table[op_index].process(argc, argv, rbuf, rsp_size);

        LOGMSG1("%s:Remote command complete, responding with:%s", __func__, rbuf);

    }

}

static void process_disable_ack(char * cbuf, char * rbuf, size_t rsp_size)
{
    rsp_ack_enabled = false;
        
#if DEBUG
    if (strlen(rsp_msg_ok) > rsp_size) {
        err_handler(ERR_TYPE_LOCAL, ERR_DEBUG);
    }
#endif
    strcpy(rbuf, rsp_msg_ok);
 
}

