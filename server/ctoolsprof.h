/*
 * ctoolsprof.h
 *
 * Ctools Profiler Server Implementation
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef CTOOLSPROF_H
#define CTOOLSPROF_H

/* Define ctoolsprof gloabls */
extern const int g_major_version;
extern const int g_minor_version;
extern const int g_copyright_year;

extern FILE *g_stdout;
//extern FILE *g_logout;
extern FILE *g_stderr;
extern char * g_whoami;

extern bool g_fifo_used;
extern bool g_fifo_enabled;
extern int g_fifo_fd;
extern bool g_terminate;

extern bool g_interactive_mode;

/* State definitions
 * Uninitialized - have nerver called the start_command_handler().
 * Waiting - Called start_command_handler but recording may be delayed or need a signal from the user app.
 * Stopped - Was recording, but now stopped.
 * Recording - Obviously the receiver is recording.
 */
typedef enum {
    STATE_UNINITIALIZED,
    STATE_WAITING,
    STATE_STOPPED,
    STATE_RECORDING
} recording_state_t;

typedef enum {
    ETB_MODE_ONESHOT_FIXED,
    ETB_MODE_ONESHOT_CIRCULAR,
    ETB_MODE_DRAIN,
    ETB_MODE_LAST
} etb_mode_t;

typedef struct {
    bool delay_active;
    bool duration_active;
    bool signal_active;
    bool etb_active;
    bool socket_disconnect;
    etb_mode_t etb_mode;
    recording_state_t recording;
} monitor_state_t;

extern monitor_state_t global_state;

void clean_up();

#endif
