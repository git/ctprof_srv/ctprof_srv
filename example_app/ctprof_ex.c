/*
 * ctprof_ex.c
 *
 * Ctools Profiler Example Implementation
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <getopt.h>
#include <stdint.h>
#include <sys/mman.h>
#include <fcntl.h>
#include "ctprof_utility.h"

const int g_major_version = 1;
const int g_minor_version = 1;
const int g_copyright_year = 2013;

FILE *g_stdout;
FILE *g_stderr;
char * g_whoami;

struct memory_blk_t{
    char * name;
    uint32_t phy_addr;
    void * v_addr;
    size_t phy_size;
}; 

struct memory_blk_t memory_table[] = {
    {"Corepac L2_0", 0x10800000, 0, 1024*1024}, /* Corepac 0 L2 */
    {"Corepac L2_1", 0x11800000, 0, 1024*1024}, /* Corepac 1 L2 */
    {"Corepac L2_2", 0x12800000, 0, 1024*1024}, /* Corepac 2 L2 */
    {"Corepac L2_3", 0x13800000, 0, 1024*1024}  /* Corepac 3 L2 */
};

const int zero_test_words = 65536;
const int memory_table_elements = sizeof(memory_table)/sizeof(struct memory_blk_t);

static struct option long_options[] = {
    {"interations", required_argument, 0, 'i'},
    {"pipe", no_argument, 0, 'P'},
    {"quiet", no_argument, 0,'q'},
    {"help", no_argument, 0, 'h'},
    {"version", no_argument, 0, 'v'}
};

static char * short_options = "i:qhvP";

static int test_iterations = 1;
static bool pipe_enable = false;

int main(int argc, char *argv[])
{
    /* Intialize globals */
    g_stdout = stdout;
    g_stderr = stderr;
    g_whoami = argv[0];

    /* evaluate commamnd line */
    while (1) {

        int option, option_index = 0;

        option = getopt_long(argc, argv, short_options, long_options, &option_index);
        
        if (option == -1) break;

        switch (option) {
        case 'i':
            test_iterations = atoi(optarg);
            break;
        case 'q':
            g_stdout = fopen("/dev/null", "w");
            break;
        case 'h':
            fprintf(g_stdout, "Usage: ctprof_ex [ihqv]\n");
            fprintf(g_stdout, " --iterations/-i <n>     Run test n times\n");
            fprintf(g_stdout, " --pipe/-P               Use pipe to coordiante with ctprof_srv\n");
            fprintf(g_stdout, " --help/-h               Print this\n");
            fprintf(g_stdout, " --quiet/-q              Send stdout to /dev/null\n");
            fprintf(g_stdout, " --version/-v            Print version\n");
            fprintf(g_stdout, "\n");
            exit(0);
        case 'P':
            pipe_enable = true;
            break;
        case 'v':
            fprintf(g_stdout, "ctprof_ex version %d.%d\n", 
                                             g_major_version, g_minor_version);
            fprintf(g_stdout, "Copyright (C) %d Texas Instruments, Inc.\n", 
                                                             g_copyright_year); 
            exit(0);
        default:
            fprintf(g_stderr,"Invalid option - try -h\n");
            exit(0); 
        } /* End of switch */

    }/* end of while*/

    /************************************************************/
    /* Wait until ctprof is recording to start                  */
    /************************************************************/
    if (pipe_enable) {
        if (ctprof_pipe_open() == -1) {
            fprintf(g_stderr, "Can't open pipe to ctprof\n");
            exit(-1);
        }
 
        ctprof_ready_wait();
        ctprof_start_recording();
        ctprof_recording_wait();
    }
    /************************************************************/
    /* Map each memory table element                            */
    /************************************************************/
    int mem_fd = open("/dev/mem", O_RDWR | O_SYNC | O_RSYNC );
    if (mem_fd == -1) {
        fprintf(g_stderr, "Can't open /dev/mem\n");
        exit(-1);
    }

    for (int i = 0; i <  memory_table_elements; i++) {
        memory_table[i].v_addr = mmap(0, memory_table[i].phy_size, 
                                       PROT_READ | PROT_WRITE, MAP_SHARED, mem_fd, 
                                       memory_table[i].phy_addr);

        mlock(memory_table[i].v_addr, memory_table[i].phy_size); 
    }   

    /************************************************************/
    /* Test Loop                                                */
    /************************************************************/
    while (test_iterations != 0) {

        fprintf(g_stdout, "\r%s:Test Iterations left %d\n", g_whoami, test_iterations);

        /* This test writes zero to the first 64K words of each memory_table element */
        for (int i = 0; i <  memory_table_elements; i++) {

            if (pipe_enable) {
                if (i == 2) {
                    ctprof_end_recording();
                    ctprof_stopped_wait();
                }
            }

            fprintf(g_stdout, "\r%s:Testing %d words of %s\n", g_whoami, zero_test_words, memory_table[i].name);

            uint32_t * addr = (uint32_t *)memory_table[i].v_addr;
            for (int n = 0; n < zero_test_words; n++) {
                 *addr++ = 0;
            }

            addr = (uint32_t *)memory_table[i].v_addr;
            int test_failed_cnt = 0;

            for (int n = 0; n < zero_test_words; n++) {
                 if (*addr++ != 0 ) {
                    test_failed_cnt++;
                }
            }

            fprintf(g_stdout, "\r%s:%s had %d failures\n", g_whoami, memory_table[i].name, test_failed_cnt);           

        }
        test_iterations--;

    } /* End of while*/

    if (pipe_enable) {
        ctprof_pipe_close();
    }
    fprintf(g_stdout, "\r%s:Exiting\n", g_whoami);
    exit(0);
}


