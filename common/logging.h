/*
 * logging.h
 *
 * Ctools Profiler Common Implementation
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef LOGGING_H
#define LOGGING_H

extern bool g_log_enable;
extern int  g_log_level;

#ifdef CLIENT
#define LOG_FILENAME "ctprof_log.txt"
#endif
#ifdef SERVER
#define LOG_FILENAME "ctprof_srv_log.txt"
#endif

typedef enum {LOG_MSG, LOG_ERR, LOG_FUNC} log_type_t;

void log_handler(void **notused, int argc);
void log_msg( log_type_t log_type, const char * format, ...);


#define IF_LOGGING_ENABLED if (g_log_enable) 

#define LOGFUNC() if (g_log_enable && g_log_level > 0) { \
                           log_msg(LOG_FUNC,"%s", __func__); \
                       } 

#define LOGMSG0(...) if (g_log_enable && g_log_level >= 0) {\
                           log_msg(LOG_MSG, __VA_ARGS__);   \
                       } 

#define LOGMSG1(...) if (g_log_enable && g_log_level >= 1) {\
                           log_msg(LOG_MSG, __VA_ARGS__);   \
                       } 


#define LOGMSG2(...) if (g_log_enable && g_log_level >= 2) {\
                           log_msg(LOG_MSG, __VA_ARGS__);   \
                       } 


#define LOGERR(...) if (g_log_enable) { \
                           log_msg(LOG_ERR, __VA_ARGS__); \
                       } 
#endif

