#!/bin/bash

if [ "$#" -lt "1" ]; then
    echo "Usage: ctprof_sync.sh cmd [options]"
    echo "       ctprof_sync.sh will start ctprof_srv, wait for the clinet to"
    echo "       start a trace session, execute the cmd, and then wait for the"
    echo "       client to disconnect from ctprof_srv, terminating the session."
    exit
fi

# Start the server
ctprof_srv -Pt &

pipe=./ctprof_fifo

# Wait for ctprof_srv to create the pipe
while true
do 
    if [[ -p $pipe ]]; then
        break
    fi
done

# First 4 bytes from ctprof_srv is it's pid
# which can be discarded.
head -c 4 $pipe >/dev/null 

# Wait for ctprof_srv to report recording state 
while true
do
    if read line <$pipe; then
        if [[ "$line" == 'ctprof recording' ]]; then
            break
        fi
    fi
done

# Execute the argument
echo "execute:$@"
eval $@

# Wait for ctprof_srv to report client disconnect
while true
do
    if read line <$pipe; then
        if [[ "$line" == 'client terminate' ]]; then
            break
        fi
    fi
    if [[ ! -f $pipe ]]; then
        break
    fi

done

echo "ctprof_sync exiting"
